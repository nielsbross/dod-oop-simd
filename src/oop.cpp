#include <vector>

class Particle
{
    public:
    Particle(Vec3 position, Vec3 direction, Vec3 size, Vec4 color, float speed) :
    position(position) ,
    direction(direction) ,
    size(size) ,
    color(color) ,
    speed(speed) {}
    
    void set_position(Vec3 new_position) { position = new_position;}
    Vec3 get_position() { return position;}
    
    void set_size(Vec3 new_size) { size = new_size;}
    Vec3 get_size() { return size;}
    
    void set_direction(Vec3 new_direction) { direction = new_direction;}
    Vec3 get_direction() { return direction;}
    
    void set_color(Vec4 new_color) { color = new_color;}
    Vec4 get_color() { return color;}
    
    void set_speed(float new_speed) { speed = new_speed;}
    float get_speed() { return speed;}
    
    void update(float delta_time)
    {
        position += direction * speed * delta_time;
    }
    
    private:
    Vec3 position;
    Vec3 direction;
    Vec3 size;
    Vec4 color;
    float speed;
};

class ParticleSystem
{
    public:
    ParticleSystem(size_t count)
    {
        particles.reserve(count);
        
        for(size_t i = 0; i < count; i++)
        {
            particles.push_back(new Particle(
                random_position()
                , random_direction()
                , random_size()
                , random_color()
                , random_speed()));
        }
    }
    
    void update(float delta_time)
    {
        for(auto& p : particles)
        {
            p->update(delta_time);
        }
    }
    private:
    std::vector<Particle*> particles;
};