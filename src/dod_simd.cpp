#include <stdlib.h>

struct Particles
{
	int count;
    
    Vec3_4x *position;
	Vec3_4x *direction;
	Vec3_4x *size;
    Vec4_4x *color;
    
    float_4x* speed;
    
};

static void init_particles(Particles& particles, int count)
{
    particles.count = count;
    
    int actual_count = count / 4;
    particles.position = (Vec3_4x*)_mm_malloc(actual_count * sizeof(Vec3_4x), 16);
    particles.direction = (Vec3_4x*)_mm_malloc(actual_count * sizeof(Vec3_4x), 16);
    particles.size = (Vec3_4x*)_mm_malloc(actual_count * sizeof(Vec3_4x), 16);
    particles.color = (Vec4_4x*)_mm_malloc(actual_count * sizeof(Vec4_4x), 16);
    particles.speed = (float_4x*)_mm_malloc(actual_count * sizeof(float_4x), 16);
    
    for(int i = 0; i < particles.count / 4; i++)
    {
        particles.position[i] = random_position_4x();
        particles.direction[i] = random_direction_4x();
        particles.size[i] = random_size_4x();
        particles.color[i] = random_color_4x();
    }
}

static void update_particles(Particles& particles, 
                             float delta_time)
{
    for(int i = 0; i < particles.count / 4; i++)
    {
        particles.position[i] += 
            particles.direction[i] 
            * particles.speed[i] 
            * delta_time;
    }
}

static void destroy_particles(Particles& particles)
{
    _mm_free(particles.position);
    _mm_free(particles.direction);
    _mm_free(particles.size);
    _mm_free(particles.color);
    _mm_free(particles.speed);
}