#include <stdlib.h>

struct Particles
{
	int count;
    
    Vec3 *position;
	Vec3 *direction;
	Vec3 *size;
    Vec4 *color;
    
    float* speed;
    
};

static void init_particles(Particles& particles, int count)
{
    particles.count = count;
    particles.position = (Vec3*)malloc(count * sizeof(Vec3));
    particles.direction = (Vec3*)malloc(count * sizeof(Vec3));
    particles.size = (Vec3*)malloc(count * sizeof(Vec3));
    particles.color = (Vec4*)malloc(count * sizeof(Vec4));
    particles.speed = (float*)malloc(count * sizeof(float));
    
    for(int i = 0; i < particles.count; i++)
    {
        particles.position[i] = random_position();
        particles.direction[i] = random_direction();
        particles.size[i] = random_size();
        particles.color[i] = random_color();
    }
}

static void update_particles(Particles& particles, 
                             float delta_time)
{
    for(int i = 0; i < particles.count; i++)
    {
        particles.position[i] += 
            particles.direction[i] 
            * particles.speed[i] 
            * delta_time;
    }
}

static void destroy_particles(Particles& particles)
{
    free(particles.position);
    free(particles.direction);
    free(particles.size);
    free(particles.color);
    free(particles.speed);
}