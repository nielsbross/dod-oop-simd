#ifndef UTILITY_H
#define UTILITY_H

#include <Windows.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

struct Vec4 
{
    float x;
    float y;
    float z;
    float w;
};

struct Vec3 
{
    float x;
    float y;
    float z;
};

Vec3 operator *(Vec3 a, Vec3 b)
{
    Vec3 res;
    
    res.x = a.x * b.x;
    res.y = a.y * b.y;
    res.z = a.z * b.z;
    
    return res;
}


Vec3 operator *(Vec3 a, float b)
{
    Vec3 res;
    
    res.x = a.x * b;
    res.y = a.y * b;
    res.z = a.z * b;
    
    return res;
}

Vec3& operator +=(Vec3 a, Vec3 b)
{
    a.x += b.x;
    a.y += b.y;
    a.z += b.z;
    
    return a;
}

float random_zero_to_one()
{
    float divisor = 1.0f / (float)RAND_MAX;
    float result = divisor * (float)rand();
    return result;
}



float lerp(float a, float t, float b)
{
    float result = (1.0f - t) * a + t * b;
    return result;
}

float random_between(float min, float max)
{
    float result = min + lerp(min, random_zero_to_one(), max);
    return result;
}

Vec3 random_position() 
{ 
    Vec3 pos;
    
    pos.x = random_between(-20.0f, 20.0f);
    pos.y = random_between(-20.0f, 20.0f);
    pos.z = random_between(-20.0f, 20.0f);
    
    return pos;
};


Vec3 random_direction() 
{ 
    Vec3 dir;
    
    dir.x = random_zero_to_one();
    dir.y = random_zero_to_one();
    dir.z = random_zero_to_one();
    
    return dir;
};


Vec3 random_size() 
{ 
    Vec3 size;
    
    size.x = random_between(0.1f, 5.0f);
    size.y = random_between(0.1f, 5.0f);
    size.z = random_between(0.1f, 5.0f);
    
    return size;
};

Vec4 random_color() 
{ 
    Vec4 color;
    
    color.x = random_zero_to_one();
    color.y = random_zero_to_one();
    color.z = random_zero_to_one();
    color.w = 1.0f;
    
    return color;
};


float random_speed() 
{ 
    float speed;
    
    speed = random_between(0.1f, 100.0f);
    
    return speed;
};

float get_ms_in_s()
{
    DWORD ms = GetTickCount();
    
    float s = (float)ms / 1000.0f;
    
    return s;
}

typedef long long int time_tick;

time_tick get_high_res_tick()
{
    LARGE_INTEGER t;
    QueryPerformanceCounter(&t);
    return (time_tick)t.QuadPart;
}

struct Timer
{
    float last_start_time;
    float cumulative_times;
    
    time_tick last_high_res_time;
    time_tick cumulative_high_res_times;
    
    int count;
};

void start_time(Timer& t)
{
    t.last_start_time = get_ms_in_s();
    t.last_high_res_time = get_high_res_tick();
}

void end_time(Timer& t)
{
    float end_time = get_ms_in_s() - t.last_start_time;
    t.cumulative_times += end_time;
    
    time_tick end_hr_time = get_high_res_tick() - t.last_high_res_time;
    t.cumulative_high_res_times += end_hr_time;
    
    t.count++;
}

void print_timer(Timer& t, const char* msg)
{
    float avg = t.cumulative_times / (float)t.count;
    time_tick avg_hr = t.cumulative_high_res_times / t.count;
    t.cumulative_times = 0.0f;
    t.cumulative_high_res_times = 0;
    t.count = 0;
    printf("%s sec: %f hr: %lld\n", msg, avg, avg_hr);
}

#endif