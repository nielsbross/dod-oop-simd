#include "utility.h"
#include "oop.cpp"


int main(int argc, char* argv[])
{
    assert(argc > 2);
    long count = strtol(argv[1], nullptr, 10);
    long its = strtol(argv[2], nullptr, 10);
    
    float last_second_check = get_ms_in_s();
    float last_frame = last_second_check;
    float delta_time = 0.0f;
    
    ParticleSystem* system = new ParticleSystem(count);
    
    bool running = count != 0;
    
    Timer t = {};
    
    int iteration = 0;
    
    while(running)
    {
        start_time(t);
        
        system->update(delta_time);
        
        end_time(t);
        
        float end_counter = get_ms_in_s();
        if(end_counter - last_second_check >= 5.0f)
        {
            last_second_check = end_counter;
            print_timer(t, "OOP: ");
            iteration++;
            if(iteration == its)
            {
                break;
            }
        }
        
        delta_time = get_ms_in_s() - last_frame;
        last_frame = end_counter;
    }
    
    return 0;
}