#ifndef UTILITY4X_H
#define UTILITY4X_H

#include <Windows.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include <mmintrin.h>  //MMX

#include <xmmintrin.h> //SSE

#include <emmintrin.h> //SSE2

#include <pmmintrin.h> //SSE3

#include <tmmintrin.h> //SSSE3

#include <smmintrin.h> //SSE4.1

#include <nmmintrin.h> //SSE4.2


#include <wmmintrin.h> //AES

#include <immintrin.h> //AVX

#include <zmmintrin.h> //AVX512


struct float_4x
{
    __m128 v;
};

struct Vec3_4x
{
    float_4x x;
    float_4x y;
    float_4x z;
};

struct Vec4_4x
{
    float_4x x;
    float_4x y;
    float_4x z;
    float_4x w;
};

float_4x operator*(float_4x a, float_4x b)
{
    float_4x res;
    res.v = _mm_mul_ps(a.v, b.v);
    return res;
}


float_4x operator+(float_4x a, float_4x b)
{
    float_4x res;
    res.v = _mm_add_ps(a.v, b.v);
    return res;
}

float_4x& operator+=(float_4x a, float_4x b)
{
    a.v = _mm_add_ps(a.v, b.v);
    return a;
}


Vec3_4x operator *(Vec3_4x a, Vec3_4x b)
{
    Vec3_4x res;
    
    res.x = a.x * b.x;
    res.y = a.y * b.y;
    res.z = a.z * b.z;
    
    return res;
}


Vec3_4x operator *(Vec3_4x a, float_4x b)
{
    Vec3_4x res;
    
    res.x = a.x * b;
    res.y = a.y * b;
    res.z = a.z * b;
    
    return res;
}


Vec3_4x operator *(Vec3_4x a, float b)
{
    Vec3_4x res;
    
    float_4x b_4x;
    b_4x.v = _mm_set1_ps(b);
    
    res.x = a.x * b_4x;
    res.y = a.y * b_4x;
    res.z = a.z * b_4x;
    
    return res;
}

float_4x operator-(float b, float_4x a)
{
    float_4x res;
    res.v = _mm_sub_ps(a.v, _mm_set1_ps(b));
    return res;
}

Vec3_4x& operator +=(Vec3_4x a, Vec3_4x b)
{
    a.x += b.x;
    a.y += b.y;
    a.z += b.z;
    
    return a;
}

float_4x random_zero_to_one_4x()
{
    float_4x res;
    res.v = _mm_set_ps(random_zero_to_one(), random_zero_to_one(), random_zero_to_one(), random_zero_to_one());
    
    return res;
}

inline float_4x min_4x(float left, float_4x right)
{
    __m128 min = _mm_min_ps(_mm_set1_ps(left), right.v);
    float_4x res;
    res.v = min;
    return res;
}

inline float_4x lerp(float_4x a, float_4x t, float_4x b)
{
    float_4x min = min_4x(1.0, t);
    float_4x inverse_min = 1.0f - min;
    float_4x a_times_inverse = inverse_min * a;
    
    return a_times_inverse + (t * b);
}

float_4x random_between_4x(float min, float max)
{
    float_4x res;
    res.v = _mm_set_ps(random_between(min, max), random_between(min, max), random_between(min, max), random_between(min, max));
    return res;
}

Vec3_4x random_position_4x() 
{ 
    Vec3_4x pos;
    
    pos.x.v = random_between_4x(-20.0f, 20.0f).v;
    pos.y.v = random_between_4x(-20.0f, 20.0f).v;
    pos.z.v = random_between_4x(-20.0f, 20.0f).v;
    
    return pos;
};


Vec3_4x random_direction_4x() 
{ 
    Vec3_4x dir;
    
    dir.x.v = random_zero_to_one_4x().v;
    dir.y.v = random_zero_to_one_4x().v;
    dir.z.v = random_zero_to_one_4x().v;
    
    return dir;
};


Vec3_4x random_size_4x() 
{ 
    Vec3_4x size;
    
    size.x.v = random_between_4x(0.1f, 5.0f).v;
    size.y.v = random_between_4x(0.1f, 5.0f).v;
    size.z.v = random_between_4x(0.1f, 5.0f).v;
    
    return size;
};

Vec4_4x random_color_4x() 
{ 
    Vec4_4x color;
    
    color.x.v = random_zero_to_one_4x().v;
    color.y.v = random_zero_to_one_4x().v;
    color.z.v = random_zero_to_one_4x().v;
    color.w.v = _mm_set1_ps(1.0f);
    
    return color;
};


float_4x random_speed_4x() 
{ 
    float_4x speed;
    
    speed.v = random_between_4x(0.1f, 100.0f).v;
    
    return speed;
};

#endif