@echo off

set WERROR=
set DEBUG=-DDEBUG=1
set GLM=
set PRP=
set WIGNORED=-wd4820 -wd4514 -wd4365 -wd4710 -wd4668 -wd5045
set OPT=-Od
set ASM=

WHERE cl
IF %ERRORLEVEL% NEQ 0 call %VCVARSALL% x64

echo %cd%
set CommonCompilerFlags=/MD %ASM% /arch:AVX -std:c++latest -fp:fast -fp:except- -nologo %OPT% -Oi -Wall -Gm- -GR- -EHa -FC -Z7 %PRP% %WIGNORED% %DEBUG% %GLM% 
set CommonLinkerFlags= Comdlg32.lib Ole32.lib kernel32.lib user32.lib gdi32.lib winmm.lib opengl32.lib shell32.lib
set ExtraLinkerFlags=/NODEFAULTLIB:"LIBCMT" -incremental:no -opt:ref /ignore:4099

IF NOT EXIST build mkdir build
pushd build

REM 64-bit build
del *.pdb > NUL 2> NUL

echo Compilation started on: %time%
cl %CommonCompilerFlags% ../src/main_oop.cpp -Femain  /link %ExtraLinkerFlags% %CommonLinkerFlags%
echo Compilation finished on: %time%
popd